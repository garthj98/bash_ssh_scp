# Update VM
sudo apt update
sudo apt-get update
sudo apt upgrade

# Install nginx
sudo apt install nginx -y

# Start nginx
sudo systemctl start nginx

# Move index file
sudo mv ~/website/index.html /var/www/html/

# Restart nginx
sudo systemctl restart nginx