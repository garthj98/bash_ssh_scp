# Bash Scripting, SSH and SCP

this class will cover some very important topics including bash `scripting`, `ssh` and `scp`.

### Bash Scripting

Bash scripting is the ability to declaritly type bash commands that provision the machine on a file and then run it against a machine.

Provisioning a machine includes:

- making files and directories
- editing / configuring files
- installign software
- startign and stopping files
- creating init files
- sending files and code over to computer 

### SSH

SSH or secure shell is very useful to securely log into a computer at a distance.
It akkiws us to open a terminal on said computer with shell.

We can then use all our bash knowlege to configure the machine.

Main command:
```bash
# remote loggin to a machine
$ ssh <options> <user>@<machine.ip>

## Example 
$  ssh -i ~/.ssh/ch9_shared.pem ubuntu@54.74.78.126
```

SSH also allows you to run commands remotely 

```bash
# ls in a remote machine
$  ssh -i ~/.ssh/ch9_shared.pem ubuntu@54.74.78.126 ls

# you can just ass the command after the ssh user and machine 
$  ssh -i ~/.ssh/ch9_shared.pem ubuntu@54.74.78.126 ls demos
$  ssh -i ~/.ssh/ch9_shared.pem ubuntu@54.74.78.126 cat example.txt

# Create files in machine 
ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 touch file.md
```

### SCP (Secure copy)

Imagine like `mv` but with ssh keys and remote computers.

Used to move files/folders into rempte machines and loaclly. This is useful to move .sh files or folders with code that need to be installed or setup.

```bash
# Syntax 
$ scp -i <keypath> <source> <target>
# When target is remote 
$ scp -i <keypath> <source> <user>@<ip>:<path>

# Send folders
$ scp -i <keypath> -r <source> <user>@<ip>:<path>
```

chmod +x bash_script101.sh 
scp -i ~/.ssh/ch9_shared.pem ~/code/2_week/bashscript_SSH_SCP/bash_script101.sh ubuntu@54.74.78.126:~/
scp -i ~/.ssh/ch9_shared.pem -r ~/code/2_week/bashscript_SSH_SCP/website ubuntu@54.74.78.126:~/
ssh -i ~/.ssh/ch9_shared.pem ubuntu@54.74.78.126 ./bash_script101.sh  