# Set IP adress
IPADRESS=54.74.78.126

# Change permisions to make sure it is executable 
chmod +x bash_script101.sh 

# Send bash script
scp -i ~/.ssh/ch9_shared.pem ~/code/2_week/bashscript_SSH_SCP/bash_script101.sh ubuntu@$IPADRESS:~/

# Send website folder
scp -i ~/.ssh/ch9_shared.pem -r ~/code/2_week/bashscript_SSH_SCP/website ubuntu@$IPADRESS:~/

# Run bash script
ssh -i ~/.ssh/ch9_shared.pem ubuntu@$IPADRESS ./bash_script101.sh  